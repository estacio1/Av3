let Circulo = {
  raio: 0,
  get circunferencia() {
    return 2 * Math.PI * this.raio;
  },
  estaNaCirc: function (a, b) {
    raio = this.raio;
    if (Math.sqrt(Math.pow(0 - a, 2) + Math.pow(0 - b, 2)) <= raio) {
      return "Dentro";
    } else {
      return "Fora";
    }
  },
};

function circunf() {
  Circulo.raio = document.getElementById("raio").value;
  const elemCirc = document.getElementById("circ");
  elemCirc.innerHTML = Circulo.circunferencia;
  const elemEstaDen = document.getElementById("estaDentro");
  elemEstaDen.innerHTML = Circulo.estaNaCirc(
    document.getElementById("px").value,
    document.getElementById("py").value
  );
}
