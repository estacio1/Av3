function validar() {
  let names = [
    {
      answer: "n",
      name: "q01",
    },
    {
      answer: "n",
      name: "q02",
    },
    {
      answer: "y",
      name: "q03",
    },
    {
      answer: "y",
      name: "q04",
    },
    {
      answer: "y",
      name: "q05",
    },
  ];
  let answers = names.map((name) => getAnswer(name));
  let sum = 0;
  pontos = names.map((item, i) => {
    return item.answer == answers[i] ? 10 : 0;
  }, answers);
  sum = pontos.reduce((previous, current) => {
    return (total = current + previous);
  }, 0);
  document.getElementById("result").innerHTML =
    "<p>Você acertou " + sum + "</h1>";
}

function getAnswer(name) {
  var ele = document.getElementsByName(name.name);
  for (i = 0; i < ele.length; i++) {
    if (ele[i].checked) {
      return ele[i].value;
    }
  }
}
